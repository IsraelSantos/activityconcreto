package figures;


import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;

public class ConditionalShape extends Shape {
	private int[] myCachedPath = new int[8];

	/**
	 * @see Shape#fillShape(Graphics)
	 */
	protected void fillShape(Graphics graphics) {
		Rectangle r = getBounds();

		setPathPoint(0, r.x, r.y + r.height/2);
		setPathPoint(1, r.x + r.width/2, r.y);
		setPathPoint(2, r.x + r.width, r.y + r.height);
		setPathPoint(2, r.x + r.width/2,  r.y);
		setPathPoint(2, r.x + r.width/2,  r.y + r.height);

		
		graphics.fillRectangle(getBounds());
	}


	private void setPathPoint(int index, int x, int y) {
		myCachedPath[index * 2] = x;
		myCachedPath[index * 2 + 1] = x;
	}

	@Override
	protected void outlineShape(Graphics graphics) {
		// TODO Auto-generated method stub
		Rectangle r = getBounds();

		graphics.drawLine(r.x, r.y + r.height/2, r.x + r.width/2, r.y);
		graphics.drawLine(r.x, r.y + r.height/2, r.x + r.width/2, r.y + r.height-1);
		graphics.drawLine(r.x + r.width/2, r.y, r.x + r.width-1, r.y + r.height/2);
		graphics.drawLine(r.x+r.width/2, r.y+r.height-1, r.x+r.width-1,r.y+r.height/2);
		
	}
	
}