/**
 */
package activitydiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fork</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activitydiagram.ActivitydiagramPackage#getFork()
 * @model annotation="gmf.node figure='rectangle' size='5,30' color='0,0,0' label='name' label.placement='none'"
 * @generated
 */
public interface Fork extends Node {
} // Fork
