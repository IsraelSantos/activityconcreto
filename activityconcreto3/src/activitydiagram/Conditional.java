/**
 */
package activitydiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activitydiagram.ActivitydiagramPackage#getConditional()
 * @model annotation="gmf.node figure='figures.ConditionalShape' label='name' label.placement='none'"
 * @generated
 */
public interface Conditional extends Node {
} // Conditional
