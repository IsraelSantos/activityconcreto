/**
 */
package activitydiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activitydiagram.ActivitydiagramPackage#getFinalNode()
 * @model annotation="gmf.node figure='ellipse' size='30,30' color='0,0,0' label='name' label.placement='none'"
 * @generated
 */
public interface FinalNode extends Node {
} // FinalNode
