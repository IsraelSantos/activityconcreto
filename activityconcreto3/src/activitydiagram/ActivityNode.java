/**
 */
package activitydiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activitydiagram.ActivitydiagramPackage#getActivityNode()
 * @model annotation="gmf.node figure='rectangle' label='name'"
 * @generated
 */
public interface ActivityNode extends Node {
} // ActivityNode
