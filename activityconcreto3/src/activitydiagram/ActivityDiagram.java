/**
 */
package activitydiagram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link activitydiagram.ActivityDiagram#getName <em>Name</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getActivity <em>Activity</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getInit <em>Init</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getFinal <em>Final</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getConditionals <em>Conditionals</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getForks <em>Forks</em>}</li>
 *   <li>{@link activitydiagram.ActivityDiagram#getJoins <em>Joins</em>}</li>
 * </ul>
 * </p>
 *
 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram()
 * @model
 * @generated
 */
public interface ActivityDiagram extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link activitydiagram.ActivityDiagram#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link activitydiagram.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Transitions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' containment reference list.
	 * The list contents are of type {@link activitydiagram.ActivityNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity</em>' containment reference list.
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Activity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ActivityNode> getActivity();

	/**
	 * Returns the value of the '<em><b>Init</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init</em>' containment reference.
	 * @see #setInit(InitialNode)
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Init()
	 * @model containment="true" required="true"
	 * @generated
	 */
	InitialNode getInit();

	/**
	 * Sets the value of the '{@link activitydiagram.ActivityDiagram#getInit <em>Init</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init</em>' containment reference.
	 * @see #getInit()
	 * @generated
	 */
	void setInit(InitialNode value);

	/**
	 * Returns the value of the '<em><b>Final</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final</em>' containment reference.
	 * @see #setFinal(FinalNode)
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Final()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FinalNode getFinal();

	/**
	 * Sets the value of the '{@link activitydiagram.ActivityDiagram#getFinal <em>Final</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final</em>' containment reference.
	 * @see #getFinal()
	 * @generated
	 */
	void setFinal(FinalNode value);

	/**
	 * Returns the value of the '<em><b>Conditionals</b></em>' containment reference list.
	 * The list contents are of type {@link activitydiagram.Conditional}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditionals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditionals</em>' containment reference list.
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Conditionals()
	 * @model containment="true"
	 * @generated
	 */
	EList<Conditional> getConditionals();

	/**
	 * Returns the value of the '<em><b>Forks</b></em>' containment reference list.
	 * The list contents are of type {@link activitydiagram.Fork}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forks</em>' containment reference list.
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Forks()
	 * @model containment="true"
	 * @generated
	 */
	EList<Fork> getForks();

	/**
	 * Returns the value of the '<em><b>Joins</b></em>' containment reference list.
	 * The list contents are of type {@link activitydiagram.Join}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Joins</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Joins</em>' containment reference list.
	 * @see activitydiagram.ActivitydiagramPackage#getActivityDiagram_Joins()
	 * @model containment="true"
	 * @generated
	 */
	EList<Join> getJoins();

} // ActivityDiagram
