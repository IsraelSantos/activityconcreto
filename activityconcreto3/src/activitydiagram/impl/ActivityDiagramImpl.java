/**
 */
package activitydiagram.impl;

import activitydiagram.ActivityDiagram;
import activitydiagram.ActivityNode;
import activitydiagram.ActivitydiagramPackage;
import activitydiagram.Conditional;
import activitydiagram.FinalNode;
import activitydiagram.Fork;
import activitydiagram.InitialNode;
import activitydiagram.Join;
import activitydiagram.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity Diagram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getName <em>Name</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getInit <em>Init</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getFinal <em>Final</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getConditionals <em>Conditionals</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getForks <em>Forks</em>}</li>
 *   <li>{@link activitydiagram.impl.ActivityDiagramImpl#getJoins <em>Joins</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityDiagramImpl extends EObjectImpl implements ActivityDiagram {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivityNode> activity;

	/**
	 * The cached value of the '{@link #getInit() <em>Init</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInit()
	 * @generated
	 * @ordered
	 */
	protected InitialNode init;

	/**
	 * The cached value of the '{@link #getFinal() <em>Final</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal()
	 * @generated
	 * @ordered
	 */
	protected FinalNode final_;

	/**
	 * The cached value of the '{@link #getConditionals() <em>Conditionals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionals()
	 * @generated
	 * @ordered
	 */
	protected EList<Conditional> conditionals;

	/**
	 * The cached value of the '{@link #getForks() <em>Forks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForks()
	 * @generated
	 * @ordered
	 */
	protected EList<Fork> forks;

	/**
	 * The cached value of the '{@link #getJoins() <em>Joins</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJoins()
	 * @generated
	 * @ordered
	 */
	protected EList<Join> joins;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityDiagramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActivitydiagramPackage.Literals.ACTIVITY_DIAGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActivitydiagramPackage.ACTIVITY_DIAGRAM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this, ActivitydiagramPackage.ACTIVITY_DIAGRAM__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivityNode> getActivity() {
		if (activity == null) {
			activity = new EObjectContainmentEList<ActivityNode>(ActivityNode.class, this, ActivitydiagramPackage.ACTIVITY_DIAGRAM__ACTIVITY);
		}
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialNode getInit() {
		return init;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInit(InitialNode newInit, NotificationChain msgs) {
		InitialNode oldInit = init;
		init = newInit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT, oldInit, newInit);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInit(InitialNode newInit) {
		if (newInit != init) {
			NotificationChain msgs = null;
			if (init != null)
				msgs = ((InternalEObject)init).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT, null, msgs);
			if (newInit != null)
				msgs = ((InternalEObject)newInit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT, null, msgs);
			msgs = basicSetInit(newInit, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT, newInit, newInit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FinalNode getFinal() {
		return final_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFinal(FinalNode newFinal, NotificationChain msgs) {
		FinalNode oldFinal = final_;
		final_ = newFinal;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL, oldFinal, newFinal);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinal(FinalNode newFinal) {
		if (newFinal != final_) {
			NotificationChain msgs = null;
			if (final_ != null)
				msgs = ((InternalEObject)final_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL, null, msgs);
			if (newFinal != null)
				msgs = ((InternalEObject)newFinal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL, null, msgs);
			msgs = basicSetFinal(newFinal, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL, newFinal, newFinal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Conditional> getConditionals() {
		if (conditionals == null) {
			conditionals = new EObjectContainmentEList<Conditional>(Conditional.class, this, ActivitydiagramPackage.ACTIVITY_DIAGRAM__CONDITIONALS);
		}
		return conditionals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Fork> getForks() {
		if (forks == null) {
			forks = new EObjectContainmentEList<Fork>(Fork.class, this, ActivitydiagramPackage.ACTIVITY_DIAGRAM__FORKS);
		}
		return forks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Join> getJoins() {
		if (joins == null) {
			joins = new EObjectContainmentEList<Join>(Join.class, this, ActivitydiagramPackage.ACTIVITY_DIAGRAM__JOINS);
		}
		return joins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__ACTIVITY:
				return ((InternalEList<?>)getActivity()).basicRemove(otherEnd, msgs);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT:
				return basicSetInit(null, msgs);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL:
				return basicSetFinal(null, msgs);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__CONDITIONALS:
				return ((InternalEList<?>)getConditionals()).basicRemove(otherEnd, msgs);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FORKS:
				return ((InternalEList<?>)getForks()).basicRemove(otherEnd, msgs);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__JOINS:
				return ((InternalEList<?>)getJoins()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__NAME:
				return getName();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__TRANSITIONS:
				return getTransitions();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__ACTIVITY:
				return getActivity();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT:
				return getInit();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL:
				return getFinal();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__CONDITIONALS:
				return getConditionals();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FORKS:
				return getForks();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__JOINS:
				return getJoins();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__NAME:
				setName((String)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__ACTIVITY:
				getActivity().clear();
				getActivity().addAll((Collection<? extends ActivityNode>)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT:
				setInit((InitialNode)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL:
				setFinal((FinalNode)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__CONDITIONALS:
				getConditionals().clear();
				getConditionals().addAll((Collection<? extends Conditional>)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FORKS:
				getForks().clear();
				getForks().addAll((Collection<? extends Fork>)newValue);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__JOINS:
				getJoins().clear();
				getJoins().addAll((Collection<? extends Join>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__TRANSITIONS:
				getTransitions().clear();
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__ACTIVITY:
				getActivity().clear();
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT:
				setInit((InitialNode)null);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL:
				setFinal((FinalNode)null);
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__CONDITIONALS:
				getConditionals().clear();
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FORKS:
				getForks().clear();
				return;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__JOINS:
				getJoins().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__ACTIVITY:
				return activity != null && !activity.isEmpty();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__INIT:
				return init != null;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FINAL:
				return final_ != null;
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__CONDITIONALS:
				return conditionals != null && !conditionals.isEmpty();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__FORKS:
				return forks != null && !forks.isEmpty();
			case ActivitydiagramPackage.ACTIVITY_DIAGRAM__JOINS:
				return joins != null && !joins.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ActivityDiagramImpl
