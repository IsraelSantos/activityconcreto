/**
 */
package activitydiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activitydiagram.ActivitydiagramPackage#getInitialNode()
 * @model annotation="gmf.node figure='ellipse' size='30,30' label='name' label.placement='none'"
 * @generated
 */
public interface InitialNode extends Node {
} // InitialNode
