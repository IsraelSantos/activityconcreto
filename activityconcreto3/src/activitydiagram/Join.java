/**
 */
package activitydiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Join</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activitydiagram.ActivitydiagramPackage#getJoin()
 * @model annotation="gmf.node figure='rectangle' size='5,30' color='0,0,0' label='name' label.placement='none'"
 * @generated
 */
public interface Join extends Node {
} // Join
